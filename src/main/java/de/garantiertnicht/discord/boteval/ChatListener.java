package de.garantiertnicht.discord.boteval;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;

public class ChatListener {
    private final IDiscordClient me;

    public ChatListener(IDiscordClient me) {
        this.me = me;
    }

    @EventSubscriber
    public void onMessageEvent(MessageReceivedEvent event) {
        if(event.getMessage().getMentions().contains(me.getOurUser())) {
            ExecutionEnvironment environment = new ExecutionEnvironment(event.getGuild(), event.getChannel(), event.getAuthor(), me, event.getMessage());
            environment.execute();
            environment.async(()-> {
                try {
                    event.getChannel().sendMessage(environment.getResult());
                } catch (MissingPermissionsException e) {
                    // Nothing we can do about
                } catch (DiscordException e) {
                    try {
                        event.getChannel().sendMessage(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
                    } catch (DiscordException | MissingPermissionsException e1) {
                        // Well... WHAT SHOULD I DO?
                    }
                }
            });
        }
    }
}
