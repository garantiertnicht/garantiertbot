package de.garantiertnicht.discord.boteval;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import sx.blah.discord.handle.obj.Permissions;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface Executable {
    Permissions[] permissionsRequired();
    String description();
    ActionType action();

    enum ActionType {
        KICK("User Kicked", "%d Users kicked"),
        ERROR("Action Failed", "%d Actions Failed"),
        NOTHING("", ""),
        CHANGE_PINNED("Changed Pin Status", "Pin Status of %d messages changed"),
        DELETE_MESSAGE("Message Deleted", "%d Messages Deleted"),
        REACT("Reaction(s) maybe edited.", "Changed Reactions on up to %d messages"),
        BAN("User banned", "%d Users banned.");

        public final String messageSingular;
        public final String messageCount;

        ActionType(String messageSingular, String messageCount) {
            this.messageSingular = messageSingular;
            this.messageCount = messageCount;
        }
    }
}
