package de.garantiertnicht.discord.boteval;

import java.util.List;

public class ExecutionBranchingList<T> {
    public final List<T> list;
    public final Class<T> elementClass;

    public ExecutionBranchingList(List<T> list, Class<T> elementClass) {
        this.list = list;
        this.elementClass = elementClass;
    }
}
