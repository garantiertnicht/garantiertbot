package de.garantiertnicht.discord.boteval;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import de.garantiertnicht.discord.boteval.Executable.ActionType;
import de.garantiertnicht.discord.boteval.api.Root;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.IVoidRequest;
import sx.blah.discord.util.RequestBuffer.RequestFuture;

public class  ExecutionEnvironment {
    public final IGuild guild;
    public final IChannel channel;
    public final IUser sender;
    public final IDiscordClient me;
    public final IMessage message;

    private final HashMap<Executable.ActionType, Integer> counts;
    private boolean failMark;
    private String step;

    private String result;

    private final Root root;

    public ExecutionEnvironment(IGuild guild, IChannel channel, IUser sender, IDiscordClient me, IMessage message) {
        this.guild = guild;
        this.channel = channel;
        this.sender = sender;
        this.me = me;
        this.message = message;

        counts = new HashMap<>();
        result = "";
        failMark = false;

        this.root = new Root(this);
    }

    public void execute() {
        execute(root, root, message.toString());
    }

    public String getResult() {
        String result = sender.mention() + ' ';
        result += failMark ? "FAILED!```\n" : "SUCCESS```\n";

        result += this.result.replace("```", "\"") + '\n';

        for(Executable.ActionType action : Executable.ActionType.values()) {
            if(counts.containsKey(action)) {
                int count = counts.get(action);

                if(count == 1) {
                    result += action.messageSingular + '\n';
                } else if(count >= 0) {
                    result += String.format(action.messageCount, count) + '\n';
                }
            }
        }

        if(result.length() > 1990) {
            result = result.substring(0, 1990);
        }

        return result + "```";
    }

    //TODO This is WAY too long!
    private void execute(Object currentContext, Object restorableContext, String command) {
        String[] parts = command.toString().split(" ");
        Object previousContext = currentContext;

        for(int i = 0; i < parts.length; i++) {
            failMark = false;
            String methodName = parts[i];

            if(methodName.isEmpty()) {
                continue;
            }

            // Special Cases
            if(methodName.equals("<@" + me.getOurUser().getID() + ">")) { // Someone mentioned us
                continue;
            } else if(methodName.equals("?")) {
                if(!checkCondition(currentContext)) {
                    return;
                }

                currentContext = previousContext;
                continue;
            } else if(methodName.equals("!?")) {
                if(checkCondition(currentContext)) {
                    return;
                }

                currentContext = previousContext;
                continue;
            }

            if(methodName.charAt(0) == ':') {
                markFailed("Syntax Error: Expected Method, but found Argument at " + methodName);
                return;
            } else if(methodName.charAt(0) == '.') {
                currentContext = root;
                methodName = methodName.substring(1);

                if(methodName.isEmpty()) {
                    continue;
                }
            }

            Class contextClass = currentContext.getClass();

            if(methodName.equals("help")) {
                addResultLine("Current Context: " + contextClass.getSimpleName());

                for(Method method : contextClass.getMethods()) {
                    Executable info = method.getAnnotation(Executable.class);

                    if(info == null) {
                        continue;
                    }

                    if(userHasPermissionsLocally(info.permissionsRequired())) {
                        addResultLine(String.format("%s(%s): %s", method.getName(), method.getParameterCount(), info.description()));
                    }
                }

                continue;
            }

            boolean shouldSaveResult = false;

            if(methodName.equals("*")) {
                currentContext = restorableContext;

                if(branchIfPossible(currentContext, currentContext, parts, i, false)) {
                    return;
                }

                continue;
            } else if(methodName.charAt(0) == '*') {
                shouldSaveResult = true;
                methodName = methodName.substring(1);

                if(methodName.isEmpty()) {
                    continue;
                }
            }

            int providedArguments = 0;
            for(int j = 1;;j++) {
                if(i + j >= parts.length || parts[i+j].charAt(0) != ':') {
                    break;
                }

                providedArguments++;
            }

            i += providedArguments;

            Method methodToExecute = null;

            for(Method method : contextClass.getMethods()) {
                if(method.getName().equals(methodName) && method.getParameterCount() == providedArguments) {
                    for(Class parameterClass : method.getParameterTypes()) {
                        if(parameterClass != String.class) {
                            break;
                        }
                    }

                    methodToExecute = method;
                }
            }

            if(methodToExecute == null) {
                markFailed("Command " + methodName + " not found in " + contextClass.getSimpleName());
                return;
            }

            Executable info = methodToExecute.getAnnotation(Executable.class);

            if(info == null) {
                markFailed("The method " + methodName + " is not a command.");
                return;
            }

            if(!userHasPermissionsLocally(info.permissionsRequired())) {
                markFailed("Access denied.");
                return;
            }

            String[] args = new String[providedArguments];

            for(int j = 0; j < providedArguments; j++) {
                args[j] = parts[i + j].substring(1);
            }

            try {
                previousContext = currentContext;
                currentContext = methodToExecute.invoke(currentContext, args);

                if(shouldSaveResult) {
                    restorableContext = currentContext;
                }
            } catch (IllegalAccessException e) {
                markFailed("The method " + methodName + " is not a command.");
                return;
            } catch (InvocationTargetException e) {
                markFailed("Command " + methodName + " failed! " + e.getMessage());
                return;
            }

            if(failMark) {
                return;
            }

            incrementAction(info.action());

            if(currentContext == null || methodToExecute.getReturnType() == void.class) {
                return;
            }

            if(branchIfPossible(currentContext, restorableContext, parts, i, shouldSaveResult)) {
                return;
            }
        }
    }

    private boolean checkCondition(Object context) {
        boolean result = false;

        if(context instanceof Boolean) {
            result = (Boolean) context;
        } else {
            return false;
        }

        return result;
    }

    private boolean branchIfPossible(Object currentContext, Object restorableContext, String parts[], int i, boolean branchNow) {
        if(currentContext instanceof ExecutionBranchingList) {
            String messageLeft = "";
            for(int j = i + 1; j < parts.length; j++) {
                messageLeft += parts[j] + ' ';
            }

            ExecutionBranchingList context = (ExecutionBranchingList) currentContext;

            if(context.list.isEmpty()) {
                markFailed(StandartErrorTexts.EMPTY.text);
                return true;
            }

            for(Object subContext : context.list) {
                execute(context.elementClass.cast(subContext), branchNow ? subContext : restorableContext, messageLeft);
            }

            return true;
        }

        return false;
    }

    public boolean userHasPermissionsLocally(Permissions... permissions) {
        return userHasPermissionsLocally(sender, channel, permissions);
    }

    public boolean userHasPermissionsGlobally(Permissions... permissions) {
        return userHasPermissionsGlobally(sender, permissions);
    }

    public boolean userHasPermissionsLocally(IUser user, IChannel channel, Permissions... permissions) {
        return checkPermissions(user, channel.getModifiedPermissions(sender), permissions);
    }

    public boolean userHasPermissionsGlobally(IUser user, Permissions... permissions) {
        return checkPermissions(user, sender.getPermissionsForGuild(guild), permissions);
    }

    private boolean checkPermissions(IUser user, EnumSet<Permissions> provider, Permissions... permissions) {
        for(Permissions permission : permissions) {
            if(!channel.getModifiedPermissions(sender).contains(permission)) {
                return false;
            }
        }

        return true;
    }

    public void markFailed(String message) {
        failMark = true;
        addResultLine(message);
    }

    public void addResultLine(String message) {
        result += message + '\n';
    }

    public RequestFuture<Void> async(IVoidRequest request) {
        return RequestBuffer.request(request);
    }

    private void incrementAction(Executable.ActionType action) {
        if(action == ActionType.NOTHING) {
            return;        }

        if(counts.containsKey(action)) {
            counts.put(action, counts.get(action) + 1);
        } else {
            counts.put(action, 1);
        }
    }
}
