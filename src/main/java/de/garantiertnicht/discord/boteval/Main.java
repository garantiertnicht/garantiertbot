package de.garantiertnicht.discord.boteval;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;

public class Main {
    public static void main(String... args) {
        if(args.length != 1) {
            System.err.println("Usage: <token>");
            System.exit(1);
            return;
        }

        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.withToken(args[0]);

        IDiscordClient client;

        try {
            client = clientBuilder.login();
        } catch (DiscordException e) {
            System.err.println("Authentication Failed!");
            System.exit(2);
            return;
        }

        client.getDispatcher().registerListener(new ChatListener(client));
    }
}
