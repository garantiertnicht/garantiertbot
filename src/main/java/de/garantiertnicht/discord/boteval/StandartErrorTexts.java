package de.garantiertnicht.discord.boteval;

/**
 * Created by garantiertnicht on 07.01.17.
 */
public enum StandartErrorTexts {
    COMMAND_NOT_FOUND("Command %s not found in %s"),
    INSUFICIENT_PERMISSIONS("Access to command %s in %s denied."),
    DISCORD_EXCEPTION("Internal Error! %s"),
    BOT_PERMISSIONS("Bot Permissions denied."),
    EMPTY("Result is empty.");

    public final String text;

    StandartErrorTexts(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
