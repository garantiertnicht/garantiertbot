package de.garantiertnicht.discord.boteval.api;

import java.util.LinkedList;

import de.garantiertnicht.discord.boteval.Executable;
import de.garantiertnicht.discord.boteval.Executable.ActionType;
import de.garantiertnicht.discord.boteval.ExecutionBranchingList;
import de.garantiertnicht.discord.boteval.ExecutionEnvironment;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;

public class Channel {
    public final ExecutionEnvironment environment;
    public final IChannel channel;

    public Channel(ExecutionEnvironment environment) {
        this.environment = environment;
        this.channel = environment.channel;
    }

    public Channel(ExecutionEnvironment environment, IChannel channel) {
        this.environment = environment;
        this.channel = channel;
    }

    @Executable(permissionsRequired = {Permissions.READ_MESSAGE_HISTORY}, description = "Gets the last n Messages.", action = ActionType.NOTHING)
    public ExecutionBranchingList<Message> messages(String numberString) {
        int number = Integer.valueOf(numberString);

        environment.async(()->channel.getMessages().load(number / 99 + 1)).get();

        LinkedList<Message> result = new LinkedList<>();

        for(IMessage message : channel.getMessages()) {
            if(result.size() < number) {
                result.add(new Message(environment, message));
            }
        }

        return new ExecutionBranchingList<>(result, Message.class);
    }

    @Executable(permissionsRequired = {}, description = "Gets all users who can read this channel as ChannelUsers", action = ActionType.NOTHING)
    public ExecutionBranchingList<ChannelUser> readers() {
        LinkedList<ChannelUser> result = new LinkedList<>();

        for(IUser user : channel.getUsersHere()) {
            result.add(new ChannelUser(environment, channel, user));
        }

        return new ExecutionBranchingList<>(result, ChannelUser.class);
    }
}
