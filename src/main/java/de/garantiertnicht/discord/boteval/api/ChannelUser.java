package de.garantiertnicht.discord.boteval.api;

import de.garantiertnicht.discord.boteval.Executable;
import de.garantiertnicht.discord.boteval.Executable.ActionType;
import de.garantiertnicht.discord.boteval.ExecutionEnvironment;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;

public class ChannelUser extends GuildUser {
    public final IChannel channel;

    public ChannelUser(ExecutionEnvironment environment, IChannel channel, IUser user) {
        super(environment, user);
        this.channel = channel;
    }

    @Override
    @Executable(permissionsRequired = {}, description = "Checks if the User has the given Permission in this channel", action = ActionType.NOTHING)
    public boolean hasPermission(String permissionName) {
        try {
            Permissions permission = Permissions.valueOf(permissionName);
            return environment.userHasPermissionsLocally(user, channel, permission);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Executable(permissionsRequired = {}, description = "Gets the GuildUser from this ChannelUser", action = ActionType.NOTHING)
    public GuildUser guildUser() {
        return new GuildUser(environment, user);
    }
}
