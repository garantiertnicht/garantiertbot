package de.garantiertnicht.discord.boteval.api;

import de.garantiertnicht.discord.boteval.Executable;
import de.garantiertnicht.discord.boteval.Executable.ActionType;
import de.garantiertnicht.discord.boteval.ExecutionEnvironment;
import de.garantiertnicht.discord.boteval.StandartErrorTexts;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

import static sx.blah.discord.handle.obj.Permissions.ADMINISTRATOR;
import static sx.blah.discord.handle.obj.Permissions.BAN;
import static sx.blah.discord.handle.obj.Permissions.KICK;

public class GuildUser {
    public final ExecutionEnvironment environment;
    public final IUser user;

    public GuildUser(ExecutionEnvironment environment, IUser user) {
        this.environment = environment;
        this.user = user;
    }

    @Executable(permissionsRequired={KICK}, description = "Kicks the User", action = ActionType.KICK)
    public void kick() {
        environment.async(() -> {
            try {
                if(user.equals(environment.me.getOurUser())) {
                    environment.markFailed("I won't kick myself for security considerations.");
                    return;
                } else {
                    environment.guild.kickUser(user);
                }
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            }
        });
    }

    @Executable(permissionsRequired = {ADMINISTRATOR, BAN}, description = "Bans the user", action = ActionType.BAN)
    public void ban() {
        ban(0);
    }

    @Executable(permissionsRequired = {ADMINISTRATOR, BAN}, description = "Bans the user and remove messages from him in the last the given number of days", action = ActionType.BAN)
    public void ban(String days) {
        ban(Integer.valueOf(days));
    }

    private void ban(int days) {
        environment.async(() -> {
            try {
                environment.guild.banUser(user);
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.BOT_PERMISSIONS.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }

    @Executable(permissionsRequired = {}, description = "The Name and Snowflake of the User", action = ActionType.NOTHING)
    public void name() {
        environment.addResultLine(user.getName() + '#' + user.getDiscriminator());
        if(user.getNicknameForGuild(environment.guild).isPresent()) {
            environment.addResultLine("  Nickname: " + user.getNicknameForGuild(environment.guild));
        }
    }

    @Executable(permissionsRequired = {}, description = "Returns if the User is a Bot", action = ActionType.NOTHING)
    public boolean isBot() {
        return user.isBot();
    }

    @Executable(permissionsRequired = {}, description = "Checks if the User has the given Permission", action = ActionType.NOTHING)
    public boolean hasPermission(String permissionName) {
        try {
            Permissions permission = Permissions.valueOf(permissionName);
            return environment.userHasPermissionsGlobally(user, permission);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Executable(permissionsRequired = {}, description = "Checks if the User Mentioned is the same", action = ActionType.NOTHING)
    public boolean is(String mention) {
        return mention.equals(user.mention(true)) || mention.equals(user.mention(false));
    }
}
