package de.garantiertnicht.discord.boteval.api;

import java.util.LinkedList;

import de.garantiertnicht.discord.boteval.Executable;
import de.garantiertnicht.discord.boteval.Executable.ActionType;
import de.garantiertnicht.discord.boteval.ExecutionEnvironment;
import de.garantiertnicht.discord.boteval.StandartErrorTexts;
import sx.blah.discord.handle.impl.obj.Emoji;
import sx.blah.discord.handle.impl.obj.Reaction;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class Message {
    public final ExecutionEnvironment environment;
    public final IMessage message;

    public Message(ExecutionEnvironment environment, IMessage message) {
        this.environment = environment;
        this.message = message;
    }

    @Executable(permissionsRequired = {Permissions.READ_MESSAGES, Permissions.READ_MESSAGE_HISTORY}, description = "Writes the message content", action = ActionType.NOTHING)
    public void write() {
        environment.addResultLine(message.getAuthor().getDisplayName(environment.guild) + ": " + message.toString());
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_MESSAGES}, description = "Deletes the Message", action = ActionType.DELETE_MESSAGE)
    public void delete() {
        environment.async(() -> {
            try {
                message.delete();
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }

    @Executable(permissionsRequired = {}, description = "Gets the author of this message", action = ActionType.NOTHING)
    public GuildUser author() {
        return new ChannelUser(environment, message.getChannel(), message.getAuthor());
    }

    @Executable(permissionsRequired = {}, description = "Returns if the message is pinner", action = ActionType.NOTHING)
    public boolean pinned() {
        return message.isPinned();
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_MESSAGES}, description = "Pins this message", action = ActionType.CHANGE_PINNED)
    public void pin() {
        if (pinned()) {
            return;
        }

        environment.async(() -> {
            try {
                environment.channel.pin(message);
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_MESSAGES}, description = "Unpins this message", action = ActionType.CHANGE_PINNED)
    public void unpin() {
        if (!pinned()) {
            return;
        }

        environment.async(() -> {
            try {
                environment.channel.unpin(message);
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_MESSAGES, Permissions.ADD_REACTIONS}, description = "Adds the given reaction to the message", action = ActionType.REACT)
    public void react(String name) {
        IEmoji emoji = environment.guild.getEmojiByName(name);
        if (emoji == null) {
            environment.markFailed("The emoji " + name + " does not exist.");
        }

        environment.async(() -> {
            try {
                message.addReaction(environment.guild.getEmojiByName(name));
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_MESSAGES, Permissions.ADD_REACTIONS}, description = "Adds the given reaction to the message", action = ActionType.REACT)
    public void unreact(String name) {
        IEmoji emoji = environment.guild.getEmojiByName(name);
        if (emoji == null) {
            environment.markFailed("The emoji " + name + " does not exist.");
        }

        LinkedList<IUser> ourUser = new LinkedList<IUser>();
        ourUser.add(environment.me.getOurUser());

        environment.async(() -> {
            try {
                message.removeReaction(message.getReactionByIEmoji(emoji));
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_MESSAGES, Permissions.ADD_REACTIONS}, description = "Adds the given reaction to the message", action = ActionType.REACT)
    public void removeAllReactions() {
        environment.async(() -> {
            try {
                message.removeAllReactions();
            } catch (DiscordException e) {
                environment.markFailed(String.format(StandartErrorTexts.DISCORD_EXCEPTION.text, e.getMessage()));
            } catch (MissingPermissionsException e) {
                environment.markFailed(StandartErrorTexts.BOT_PERMISSIONS.text);
            }
        });
    }
}