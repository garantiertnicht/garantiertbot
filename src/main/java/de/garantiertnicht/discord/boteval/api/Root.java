package de.garantiertnicht.discord.boteval.api;

import java.util.LinkedList;

import de.garantiertnicht.discord.boteval.Executable;
import de.garantiertnicht.discord.boteval.Executable.ActionType;
import de.garantiertnicht.discord.boteval.ExecutionBranchingList;
import de.garantiertnicht.discord.boteval.ExecutionEnvironment;
import de.garantiertnicht.discord.boteval.StandartErrorTexts;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;

public class Root {
    public final ExecutionEnvironment environment;

    public Root(ExecutionEnvironment environment) {
        this.environment = environment;
    }

    @Executable(permissionsRequired = {}, description = "A Reference to the Message Sender", action = ActionType.NOTHING)
    public GuildUser sender() {
        return new GuildUser(environment, environment.sender);
    }

    @Executable(permissionsRequired = {Permissions.MANAGE_SERVER}, description = "A Reference to me (bot)", action = ActionType.NOTHING)
    public GuildUser botUser() {
        return new GuildUser(environment, environment.me.getOurUser());
    }

    @Executable(permissionsRequired = {}, description = "All Users connected to this Guild", action = ActionType.NOTHING)
    public ExecutionBranchingList<GuildUser> users() {
        LinkedList<GuildUser> result = new LinkedList<>();

        for (IUser user : environment.guild.getUsers()) {
            result.add(new GuildUser(environment, user));
        }

        return new ExecutionBranchingList<>(result, GuildUser.class);
    }

    @Executable(permissionsRequired = {}, description = "Gets the current channel", action = ActionType.NOTHING)
    public Channel channel() {
        return new Channel(environment);
    }

    @Executable(permissionsRequired = {Permissions.ADMINISTRATOR}, description = "Gets the channel with the given name", action = ActionType.NOTHING)
    public Channel channel(String channel) {
        if(!environment.userHasPermissionsGlobally(Permissions.ADMINISTRATOR)) { // We require a Global Administrator Permission
            environment.markFailed(String.format(StandartErrorTexts.INSUFICIENT_PERMISSIONS.text, "channel", this.getClass().getSimpleName()));
            return null;
        }

        for(IChannel channelToSwitch : environment.guild.getChannels()) {
            if(channelToSwitch.mention().equals(channel)) {
                return new Channel(environment, channelToSwitch);
            }
        }

        environment.markFailed("Channel " + channel + " not found.");
        return null;
    }
}
